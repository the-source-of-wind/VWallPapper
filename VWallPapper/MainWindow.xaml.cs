﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WinAPI;

namespace VWallPapper
{
    public partial class MainWindow : Window
    {
        //public string path=Environment.CurrentDirectory;
        private static NotifyIcon trayIcon;
        private WallPapperShow window = new WallPapperShow();
        WindowState ws;
        WindowState wsl;
        int ShowModel = 1;
        int showmodel = 0;
        public MainWindow()
        {
            this.ShowInTaskbar = false;
            InitializeComponent();
            window.Show();
            wsl = WindowState;
            Target_Text.Visibility = Visibility.Hidden;
            Target_URL.Visibility = Visibility.Visible;
        }
       private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //e.Cancel = true;
            Environment.Exit(0);
            //window.Close();
            //this.Visibility = Visibility.Hidden;
        }

        private void RemoveTrayIcon()
        {
            if (trayIcon != null)
            {
                trayIcon.Visible = false;
                trayIcon.Dispose();
                trayIcon = null;
            }
        }
        private void AddTrayIcon()
        {
            if (trayIcon != null)
            {
                return;
            }
            trayIcon = new NotifyIcon
            {
                Icon = Properties.Resources.amekoi_special2,
                Text = "NotifyIconStd"
            };
            trayIcon.Visible = true;
            trayIcon.MouseClick += TrayIcon_MouseClick;

            System.Windows.Forms.ContextMenu menu = new System.Windows.Forms.ContextMenu();

            System.Windows.Forms.MenuItem closeItem = new System.Windows.Forms.MenuItem();
            closeItem.Text = "关闭";
            closeItem.Click += new EventHandler(delegate
            {
                RemoveTrayIcon();
                Environment.Exit(0);
            });

            menu.MenuItems.Add(closeItem);

            trayIcon.ContextMenu = menu;    //设置NotifyIcon的右键弹出菜单
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //System.Environment.CurrentDirectory;
            string load_path = "";
            switch (showmodel) {
                case 0:
                    load_path="file:///" + Target_URL.Text.Replace("\\", "/");// + Target.Text;
                    break;
                case 1:
                    load_path = "http://"+Target_Text.Text;
                    break;
            }
            window.Viewer.Navigate(load_path);
            Config.SavePath(load_path);

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string init_path=Config.GetOldConfigAddress();
            AddTrayIcon();
        }
        private void TrayIcon_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.Visibility = Visibility.Visible;
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            ws = WindowState;
            if (ws == WindowState.Minimized)
            {
                this.Visibility = Visibility.Hidden;
            }
        }

        private void Window_Drop(object sender, System.Windows.DragEventArgs e)
        {
            string msg = "Drop";
            if (e.Data.GetDataPresent(System.Windows.DataFormats.FileDrop))
            {
                msg = ((System.Array)e.Data.GetData(System.Windows.DataFormats.FileDrop)).GetValue(0).ToString();
            }
            Target_URL.Text = msg;
            //System.Windows.MessageBox.Show(msg);
        }

        private void Model_Change(object sender, RoutedEventArgs e) {
            switch (ShowModel) {
                case 0:
                    Target_Text.Text = "请输入目标URL";
                    Target_Text.Visibility = Visibility.Hidden;
                    Target_URL.Visibility = Visibility.Visible;
                    ShowModel = 1;
                    showmodel = 0;
                    break;
                case 1:
                    Target_Text.Visibility = Visibility.Visible;
                    Target_URL.Visibility = Visibility.Hidden;
                    ShowModel = 0;
                    showmodel = 1;
                    break;
            }
        }
    }
}
