﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinWallpaper.Utils
{
    public static class WallpaperUtils
    {
        /// <summary>
        /// 获取窗口句柄
        /// </summary>
        /// <returns></returns>
        public static IntPtr GetWorkerW()
        {
            IntPtr windowHandle =WinAPI.Win32API.User32.FindWindow("Progman", null);

            IntPtr zero;
            WinAPI.Win32API.User32.SendMessageTimeout(windowHandle, 0x52c, new IntPtr(0), IntPtr.Zero, WinAPI.Win32API.User32.SendMessageTimeoutFlags.SMTO_NORMAL, 0x3e8, out zero);
            IntPtr workerw = IntPtr.Zero;
            WinAPI.Win32API.User32.EnumWindows(delegate (IntPtr tophandle, IntPtr topparamhandle)
            {
                if (WinAPI.Win32API.User32.FindWindowEx(tophandle, IntPtr.Zero, "SHELLDLL_DefView", null) != IntPtr.Zero)
                {
                    workerw = WinAPI.Win32API.User32.FindWindowEx(IntPtr.Zero, tophandle, "WorkerW", null);
                }
                return true;
            }, IntPtr.Zero);
            WinAPI.Win32API.User32.ShowWindow(workerw, WinAPI.Win32API.User32.SW_HIDE);
            return windowHandle;
        }
    }
}
